var styles = ["Джаз", "Блюз"];
styles[1] = "Классика";
styles.push("Рок-н-ролл");
document.write(styles.join(" ") + "<hr />");
var firstElement = styles.shift();
document.write("<p>Удаленный первый элемент: " + firstElement + "<hr />");
styles.unshift("Рэп", "Регги", "Джаз");
document.write(styles.join(" ") + "<hr />");
